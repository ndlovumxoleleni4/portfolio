document.addEventListener("DOMContentLoaded", function () {
    var button = document.getElementById("show-more");
    var moreInfo = document.getElementById("more-info");

    button.addEventListener("click", function () {
        if (moreInfo.classList.contains("hidden")) {
            moreInfo.classList.remove("hidden");
            button.innerHTML = "Show Less";
        } else {
            moreInfo.classList.add("hidden");
            button.innerHTML = "Show More";
        }
    });

});
